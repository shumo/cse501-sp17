/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 *
 * Rewritten in TypeScript and extended
 * ⓒ2017, Alex Polozov and the University of Washington.
 */

//// The AST

// This class represents all AST Nodes
class ASTNode {
    type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Unimplemented AST node " + this.type);
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 Implement call-chaining
    filter(p:(any) => any): ASTNode {
        return new ThenNode(this, new FilterNode(p));
    }

    apply(f: (any) => any): ASTNode{
        return new ThenNode(this, new ApplyNode(f));
    }

    count(): ASTNode {
        return new ThenNode(this, new CountNode());
    }

    product(r: ASTNode): ASTNode {
        return new CartesianProductNode(this, r);
    }

    join(r: ASTNode, f: ((l:any, r:any) => boolean) | string): ASTNode{
        if (typeof f === "string"){
            let fstr = <string> f;
            var fp = <FieldPredicate> function(x):boolean{
                return x.left[fstr] == x.right[fstr]};
            fp.field = fstr;
            return this.product(r).filter(fp).apply(merge);
        }
        let ff = <(l:any, r:any) => boolean> f;
        return this.product(r).filter(x => ff(x.left, x.right)).apply(merge);
    }
}

interface FieldPredicate{
    (x:any): boolean;
    field: string;
}

// The ALL node just outputs all records.
class AllNode extends ASTNode {

    constructor() {
        super("All");
    }

    //// 1.1 implement execute
    execute(data: any[]): any[] {
        return data;
    }
}

// The Filter node uses a callback to throw out some records
class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    execute(data: any[]): any[]{
        return data.filter(this.predicate);
    }
}

// The Then node chains multiple actions on one data structure
class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    execute(data: any[]): any[]{
        let inter = this.first.execute(data);
        return this.second.execute(inter);
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}


//// 1.2 Write a Query
// Define the `theftsQuery` and `autoTheftsQuery` variables

let theftsQuery = new FilterNode(x => x[13].match(/THEFT/));
let autoTheftsQuery = new FilterNode(x => x[13].match(/^VEH-THEFT/));

//// 1.3 Add Apply and Count Nodes
// ...
class ApplyNode extends ASTNode {
    applyFun: (datum: any) => any;

    constructor(applyFun: (any)=>any){
        super("Apply");
        this.applyFun = applyFun;
    }

    execute(data: any[]): any[] {
        return data.map(this.applyFun);
    }
}

class CountNode extends ASTNode {
    constructor(){
        super("Count");
    }

    execute(data: any[]): any[]{
        return [data.length];
    }
}

//// 1.4 Clean the data

interface CleanData{
    type: string;
    date: Date;
    description: string;
    area: string
}

function extractCleanData(x: any[]){
    return {type: x[13], description: x[15], date: x[17], area: x[19]};
}

let cleanupQuery = new ApplyNode(extractCleanData);

let Q = new AllNode();

//// 1.6 Reimplement queries with call-chaining

let cleanupQuery2 = Q.apply(extractCleanData); // ...

let theftsQuery2 = Q.filter(x => x.type.match(/THEFT/)); // ...

let autoTheftsQuery2 = Q.filter(x => x.type.match(/^VEH-THEFT/)); // ...

//// 2.1 Optimize Queries

function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    let old = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function (this: ASTNode): ASTNode {
        let newThis = old.call(this);
        return opt.call(newThis) || newThis;
    }
}

// ...

AddOptimization(ThenNode, function(){
    let newthis = <ThenNode> this;
    let fst = newthis.first;
    let snd = newthis.second;
    if (fst instanceof ThenNode){
        let newfst = <ThenNode> fst;
        if(newfst.second instanceof FilterNode && snd instanceof FilterNode){
            let fn1 = <FilterNode> newfst.second;
            let fn2 = <FilterNode> snd;
            let newfn = new FilterNode(x => fn1.predicate(x) && fn2.predicate(x));
            return new ThenNode(newfst.first, newfn);
        }
    }
    return this; 
});

//// 2.2 Internal node types and CountIf
// ...

class CountIfNode extends ASTNode {
    predicate: (any) => boolean;

    constructor(predicate: (any) => boolean){
        super("CountIf");
        this.predicate = predicate;
    }

    execute(data: any[]): any[]{
        return [data.filter(this.predicate).length];
    }
}

AddOptimization(ThenNode, function(){
    let newthis = <ThenNode> this;
    let fst = newthis.first;
    let snd = newthis.second;
    if(fst instanceof ThenNode){
        let newfst = <ThenNode> fst;
        if(newfst.second instanceof FilterNode && snd instanceof CountNode){
            let fn = <FilterNode> newfst.second;
            return new ThenNode(newfst.first, new CountIfNode(fn.predicate));
        }
    }
    return this;
});

//// 3.1 Cartesian Products
// ...

//cartesian product function
function xprod(ld:any[], rd:any[]){
    var ret = [];
    for (let i of ld){
        for(let j of rd){
            ret.push({left:i, right:j});
        }
    }
    return ret;
}

class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;

    constructor(left: ASTNode, right:ASTNode){
        super("CartesianProductNode");
        this.left = left;
        this.right = right;
    }

    execute(data: any[]): any[]{
        let ld = this.left.execute(data);
        let rd = this.right.execute(data);
        return xprod(ld, rd);
    }
}

//// 3.2 Joins
// ...

function merge(x){
            let l = x.left;
            let r = x.right;
            var ret = {};
            for (let i in l){
                ret[i] = l[i];
            }
            for (let i in r){
                ret[i] = r[i]; // this will override the same attribute
            }
            return ret;
}

//// 3.3 Optimizing joins
// ...
class JoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    f: (any) => boolean;

    constructor(f: (any) => boolean, left: ASTNode, right: ASTNode){
        super("Join");
        this.f = f;
        this.left = left;
        this.right = right;
    }

    execute(data: any[]): any[]{
        let ld = this.left.execute(data);
        let rd = this.right.execute(data);
        return xprod(ld, rd).filter(this.f).map(merge);
    }
}

AddOptimization(ThenNode, function(){
    let newthis = <ThenNode> this;
    let fst = newthis.first;
    let snd = newthis.second;
    if(fst instanceof ThenNode){
        let newfst = <ThenNode> fst;
        if(newfst.first instanceof CartesianProductNode &&
           newfst.second instanceof FilterNode &&
           snd instanceof ApplyNode){
            let fn = <FilterNode> newfst.second;
            let an = <ApplyNode> snd;
            let cn = <CartesianProductNode> newfst.first
            if(an.applyFun == merge){
                if("field" in fn.predicate){
                    let fp = <FieldPredicate> fn.predicate;
                    return new HashJoinNode(fp.field, cn.left, cn.right);
                }
                return new JoinNode(fn.predicate, cn.left, cn.right);
            } 
        }
    }
    return this;
});

//// 3.4 Join on fields
// ...

//// 3.5 Implement hash joins
// ...

function makeHash(data: any[], field: string): any {
    var ret = {};
    for(let i of data){
        let k = i[field];
        if(k in ret){
            ret[k].push(i);
        } else {
            ret[k] = [i];
        }
    }
    return ret;
}

class HashJoinNode extends ASTNode{
    left: ASTNode;
    right: ASTNode;
    field: string;

    constructor(field:string, left:ASTNode, right: ASTNode){
        super("HashJoin");
        this.left = left;
        this.right = right;
        this.field = field;
    }

    execute(data:any[]): any[]{
        let ld = this.left.execute(data);
        let rd = this.right.execute(data);
        let lht = makeHash(ld, this.field);
        let rht = makeHash(rd, this.field);
        var ret = [];
        for(let key in lht){
            if(key in rht){
                let subret = xprod(lht[key], rht[key]); 
                ret = ret.concat(subret);
            }
        }
        return ret.map(merge);
    }
}

//// 3.6 Optimize joins on fields to hash joins
// ...
