/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 * ©2016, Pavel Panchekha and the University of Washington.
 * Released under the MIT license.
 *
 * Rewritten in TypeScript and extended
 * ⓒ2017, Alex Polozov and the University of Washington.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//// The AST
// This class represents all AST Nodes
var ASTNode = (function () {
    function ASTNode(type) {
        this.type = type;
    }
    ASTNode.prototype.execute = function (data) {
        throw new Error("Unimplemented AST node " + this.type);
    };
    ASTNode.prototype.optimize = function () {
        return this;
    };
    ASTNode.prototype.run = function (data) {
        return this.optimize().execute(data);
    };
    //// 1.5 Implement call-chaining
    ASTNode.prototype.filter = function (p) {
        return new ThenNode(this, new FilterNode(p));
    };
    ASTNode.prototype.apply = function (f) {
        return new ThenNode(this, new ApplyNode(f));
    };
    ASTNode.prototype.count = function () {
        return new ThenNode(this, new CountNode());
    };
    ASTNode.prototype.product = function (r) {
        return new CartesianProductNode(this, r);
    };
    ASTNode.prototype.join = function (r, f) {
        if (typeof f === "string") {
            var fstr_1 = f;
            var fp = function (x) {
                return x.left[fstr_1] == x.right[fstr_1];
            };
            fp.field = fstr_1;
            return this.product(r).filter(fp).apply(merge);
        }
        var ff = f;
        return this.product(r).filter(function (x) { return ff(x.left, x.right); }).apply(merge);
    };
    return ASTNode;
}());
// The ALL node just outputs all records.
var AllNode = (function (_super) {
    __extends(AllNode, _super);
    function AllNode() {
        return _super.call(this, "All") || this;
    }
    //// 1.1 implement execute
    AllNode.prototype.execute = function (data) {
        return data;
    };
    return AllNode;
}(ASTNode));
// The Filter node uses a callback to throw out some records
var FilterNode = (function (_super) {
    __extends(FilterNode, _super);
    function FilterNode(predicate) {
        var _this = _super.call(this, "Filter") || this;
        _this.predicate = predicate;
        return _this;
    }
    //// 1.1 implement execute
    FilterNode.prototype.execute = function (data) {
        return data.filter(this.predicate);
    };
    return FilterNode;
}(ASTNode));
// The Then node chains multiple actions on one data structure
var ThenNode = (function (_super) {
    __extends(ThenNode, _super);
    function ThenNode(first, second) {
        var _this = _super.call(this, "Then") || this;
        _this.first = first;
        _this.second = second;
        return _this;
    }
    //// 1.1 implement execute
    ThenNode.prototype.execute = function (data) {
        var inter = this.first.execute(data);
        return this.second.execute(inter);
    };
    ThenNode.prototype.optimize = function () {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    };
    return ThenNode;
}(ASTNode));
//// 1.2 Write a Query
// Define the `theftsQuery` and `autoTheftsQuery` variables
var theftsQuery = new FilterNode(function (x) { return x[13].match(/THEFT/); });
var autoTheftsQuery = new FilterNode(function (x) { return x[13].match(/^VEH-THEFT/); });
//// 1.3 Add Apply and Count Nodes
// ...
var ApplyNode = (function (_super) {
    __extends(ApplyNode, _super);
    function ApplyNode(applyFun) {
        var _this = _super.call(this, "Apply") || this;
        _this.applyFun = applyFun;
        return _this;
    }
    ApplyNode.prototype.execute = function (data) {
        return data.map(this.applyFun);
    };
    return ApplyNode;
}(ASTNode));
var CountNode = (function (_super) {
    __extends(CountNode, _super);
    function CountNode() {
        return _super.call(this, "Count") || this;
    }
    CountNode.prototype.execute = function (data) {
        return [data.length];
    };
    return CountNode;
}(ASTNode));
function extractCleanData(x) {
    return { type: x[13], description: x[15], date: x[17], area: x[19] };
}
var cleanupQuery = new ApplyNode(extractCleanData);
var Q = new AllNode();
//// 1.6 Reimplement queries with call-chaining
var cleanupQuery2 = Q.apply(extractCleanData); // ...
var theftsQuery2 = Q.filter(function (x) { return x.type.match(/THEFT/); }); // ...
var autoTheftsQuery2 = Q.filter(function (x) { return x.type.match(/^VEH-THEFT/); }); // ...
//// 2.1 Optimize Queries
function AddOptimization(nodeType, opt) {
    var old = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function () {
        var newThis = old.call(this);
        return opt.call(newThis) || newThis;
    };
}
// ...
AddOptimization(ThenNode, function () {
    var newthis = this;
    var fst = newthis.first;
    var snd = newthis.second;
    if (fst instanceof ThenNode) {
        var newfst = fst;
        if (newfst.second instanceof FilterNode && snd instanceof FilterNode) {
            var fn1_1 = newfst.second;
            var fn2_1 = snd;
            var newfn = new FilterNode(function (x) { return fn1_1.predicate(x) && fn2_1.predicate(x); });
            return new ThenNode(newfst.first, newfn);
        }
    }
    return this;
});
//// 2.2 Internal node types and CountIf
// ...
var CountIfNode = (function (_super) {
    __extends(CountIfNode, _super);
    function CountIfNode(predicate) {
        var _this = _super.call(this, "CountIf") || this;
        _this.predicate = predicate;
        return _this;
    }
    CountIfNode.prototype.execute = function (data) {
        return [data.filter(this.predicate).length];
    };
    return CountIfNode;
}(ASTNode));
AddOptimization(ThenNode, function () {
    var newthis = this;
    var fst = newthis.first;
    var snd = newthis.second;
    if (fst instanceof ThenNode) {
        var newfst = fst;
        if (newfst.second instanceof FilterNode && snd instanceof CountNode) {
            var fn = newfst.second;
            return new ThenNode(newfst.first, new CountIfNode(fn.predicate));
        }
    }
    return this;
});
//// 3.1 Cartesian Products
// ...
//cartesian product function
function xprod(ld, rd) {
    var ret = [];
    for (var _i = 0, ld_1 = ld; _i < ld_1.length; _i++) {
        var i = ld_1[_i];
        for (var _a = 0, rd_1 = rd; _a < rd_1.length; _a++) {
            var j = rd_1[_a];
            ret.push({ left: i, right: j });
        }
    }
    return ret;
}
var CartesianProductNode = (function (_super) {
    __extends(CartesianProductNode, _super);
    function CartesianProductNode(left, right) {
        var _this = _super.call(this, "CartesianProductNode") || this;
        _this.left = left;
        _this.right = right;
        return _this;
    }
    CartesianProductNode.prototype.execute = function (data) {
        var ld = this.left.execute(data);
        var rd = this.right.execute(data);
        return xprod(ld, rd);
    };
    return CartesianProductNode;
}(ASTNode));
//// 3.2 Joins
// ...
function merge(x) {
    var l = x.left;
    var r = x.right;
    var ret = {};
    for (var i in l) {
        ret[i] = l[i];
    }
    for (var i in r) {
        ret[i] = r[i]; // this will override the same attribute
    }
    return ret;
}
//// 3.3 Optimizing joins
// ...
var JoinNode = (function (_super) {
    __extends(JoinNode, _super);
    function JoinNode(f, left, right) {
        var _this = _super.call(this, "Join") || this;
        _this.f = f;
        _this.left = left;
        _this.right = right;
        return _this;
    }
    JoinNode.prototype.execute = function (data) {
        var ld = this.left.execute(data);
        var rd = this.right.execute(data);
        return xprod(ld, rd).filter(this.f).map(merge);
    };
    return JoinNode;
}(ASTNode));
AddOptimization(ThenNode, function () {
    var newthis = this;
    var fst = newthis.first;
    var snd = newthis.second;
    if (fst instanceof ThenNode) {
        var newfst = fst;
        if (newfst.first instanceof CartesianProductNode &&
            newfst.second instanceof FilterNode &&
            snd instanceof ApplyNode) {
            var fn = newfst.second;
            var an = snd;
            var cn = newfst.first;
            if (an.applyFun == merge) {
                if ("field" in fn.predicate) {
                    var fp = fn.predicate;
                    return new HashJoinNode(fp.field, cn.left, cn.right);
                }
                return new JoinNode(fn.predicate, cn.left, cn.right);
            }
        }
    }
    return this;
});
//// 3.4 Join on fields
// ...
//// 3.5 Implement hash joins
// ...
function makeHash(data, field) {
    var ret = {};
    for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
        var i = data_1[_i];
        var k = i[field];
        if (k in ret) {
            ret[k].push(i);
        }
        else {
            ret[k] = [i];
        }
    }
    return ret;
}
var HashJoinNode = (function (_super) {
    __extends(HashJoinNode, _super);
    function HashJoinNode(field, left, right) {
        var _this = _super.call(this, "HashJoin") || this;
        _this.left = left;
        _this.right = right;
        _this.field = field;
        return _this;
    }
    HashJoinNode.prototype.execute = function (data) {
        var ld = this.left.execute(data);
        var rd = this.right.execute(data);
        var lht = makeHash(ld, this.field);
        var rht = makeHash(rd, this.field);
        var ret = [];
        for (var key in lht) {
            if (key in rht) {
                var subret = xprod(lht[key], rht[key]);
                ret = ret.concat(subret);
            }
        }
        return ret.map(merge);
    };
    return HashJoinNode;
}(ASTNode));
//// 3.6 Optimize joins on fields to hash joins
// ...
//# sourceMappingURL=q.js.map