const antlr4 = require("antlr4");
const RegexLexer = require("./RegexLexer").RegexLexer;
const RegexParser = require("./RegexParser").RegexParser;
const RegexExplainer = require("./RegexExplainer").RegexExplainer;

module.exports.explainRegex = function(regex) {
    const chars = new antlr4.InputStream(regex);
    const lexer = new RegexLexer(chars);
    const tokens = new antlr4.CommonTokenStream(lexer);
    const parser = new RegexParser(tokens);
    parser.buildParseTrees = true;
    const tree = parser.regex();
    const explainer = new RegexExplainer();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(explainer, tree);

    console.log(JSON.stringify(explainer.data));
    renderExplanation(explainer.data);
};

function renderExplanation(explanation) {
    const $explanation = $("#explanation");
    const jstree = $explanation.jstree(true);
    if (jstree) {
        jstree.settings.core.data = explanation;
        jstree.refresh();
    }
    else {
        $explanation.jstree({
            "core": {
                "check_callback": true,
                "data": explanation,
                "themes": {
                    "variant": "large"
                }
            }
        });
    }
}
