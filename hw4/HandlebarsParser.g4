parser grammar HandlebarsParser;

options { language=JavaScript; tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement  : TEXT (TEXT | BRACE)* ;

commentElement : START COMMENT END_COMMENT ;

expressionElement : START expression END ;

expression returns [source]
    : literalExpression 
    | idExpression
    | OPEN_PAREN expression CLOSE_PAREN 
    | helperExpression
    ;

literalExpression returns [source]
    : literal=(INTEGER | STRING | FLOAT) ;

idExpression returns [source] : id=ID ;

helperExpression returns [source]
    : helper=ID (params+=expression)+ ;

blockElement
    : openBlock blockBody closeBlock
    ;
openBlock
    : START BLOCK blockHelper END
    ;
blockHelper
    : id=ID (params+=expression)+ 
    ;
blockBody
    : (rawElement | expressionElement | commentElement)+
    ;
closeBlock
    : START CLOSE_BLOCK ID END
    ;