export default class EngExp {
    private prefixes: string = "";
    private suffixes: string = "";
    private flags: string = "m";
    private pattern: string = "";
    private numberOfUnclosedCaps: number = 0;

    private static sanitize(s: string | EngExp): string | EngExp {
        if (s instanceof EngExp)
            return s;
        else
            return s.replace(/([\].|*?+(){}^$\\:=[])/g, "\\$&");
    }

    asRegExp(): RegExp {
        return new RegExp(this.prefixes + this.pattern + this.suffixes, this.flags);
    }

    match(literal: string): EngExp {
        return this.then(literal);
    }

    then(pattern: string | EngExp): EngExp {
        let p = pattern instanceof EngExp ? pattern.closeCaps() : pattern;
        this.pattern += `(?:${EngExp.sanitize(pattern)})`;
        return this;
    }

    startOfLine(): EngExp {
        this.prefixes = "^" + this.prefixes;
        return this;
    }

    endOfLine(): EngExp {
        this.suffixes = this.suffixes + "$";
        return this;
    }

    zeroOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.closeCaps().zeroOrMore());
        else {
            this.pattern = `(?:${this.pattern})*`;
            return this;
        }
    }

    oneOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.closeCaps().oneOrMore());
        else {
            this.pattern = `(?:${this.closeCaps().pattern})+`;
            return this;
        }
    }

    optional(): EngExp {
        this.pattern = `(?:${this.pattern})?`;
        return this;
    }

    maybe(pattern: string | EngExp): EngExp {
        let p = pattern instanceof EngExp ? pattern.closeCaps() : pattern;
        this.pattern += `(?:${EngExp.sanitize(p)})?`;
        return this;
    }

    anythingBut(characters: string): EngExp {
        this.pattern += `[^${EngExp.sanitize(characters)}]*`;
        return this;
    }

    digit(): EngExp {
        this.pattern += "\\d";
        return this;
    }

    repeated(from: number, to?: number): EngExp {
        this.pattern = to ? `(?:${this.pattern}){${from},${to}}` : `(?:${this.pattern}){${from}}`;        
        return this;
    }

    multiple(pattern: string | EngExp, from: number, to?: number) {
        let p = pattern instanceof EngExp ? pattern.closeCaps() : pattern
        this.pattern += to ? `(?:${EngExp.sanitize(p)}){${from},${to}}` : `(?:${EngExp.sanitize(p)}){${from}}`;
        return this;
    }

    or(pattern: string | EngExp): EngExp {
        // FILL IN HERE
        let p = pattern instanceof EngExp ? pattern.closeCaps() : pattern;
        this.pattern = `(?:${this.pattern}|(?:${EngExp.sanitize(p)}))`;
        return this;
    }

    beginCapture(): EngExp {
        // FILL IN HERE
        this.pattern += `(`;
        this.numberOfUnclosedCaps += 1;
        return this;
    }

    endCapture(): EngExp {
        // FILL IN HERE
        this.pattern += ')';
        return this;
    }

    toString(): string {
        return this.asRegExp().source;
    }

    valueOf(): string {
        return this.asRegExp().source;
    }

    closeCaps(): EngExp{
        while(this.numberOfUnclosedCaps > 0){
            this.pattern += ')';
            this.numberOfUnclosedCaps -= 1;
        }
        return this;
    }
}
