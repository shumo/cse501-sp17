"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EngExp = (function () {
    function EngExp() {
        this.prefixes = "";
        this.suffixes = "";
        this.flags = "m";
        this.pattern = "";
        this.numberOfUnclosedCaps = 0;
    }
    EngExp.sanitize = function (s) {
        if (s instanceof EngExp)
            return s;
        else
            return s.replace(/([\].|*?+(){}^$\\:=[])/g, "\\$&");
    };
    EngExp.prototype.asRegExp = function () {
        return new RegExp(this.prefixes + this.pattern + this.suffixes, this.flags);
    };
    EngExp.prototype.match = function (literal) {
        return this.then(literal);
    };
    EngExp.prototype.then = function (pattern) {
        var p = pattern instanceof EngExp ? pattern.closeCaps() : pattern;
        this.pattern += "(?:" + EngExp.sanitize(pattern) + ")";
        return this;
    };
    EngExp.prototype.startOfLine = function () {
        this.prefixes = "^" + this.prefixes;
        return this;
    };
    EngExp.prototype.endOfLine = function () {
        this.suffixes = this.suffixes + "$";
        return this;
    };
    EngExp.prototype.zeroOrMore = function (pattern) {
        if (pattern)
            return this.then(pattern.closeCaps().zeroOrMore());
        else {
            this.pattern = "(?:" + this.pattern + ")*";
            return this;
        }
    };
    EngExp.prototype.oneOrMore = function (pattern) {
        if (pattern)
            return this.then(pattern.closeCaps().oneOrMore());
        else {
            this.pattern = "(?:" + this.closeCaps().pattern + ")+";
            return this;
        }
    };
    EngExp.prototype.optional = function () {
        this.pattern = "(?:" + this.pattern + ")?";
        return this;
    };
    EngExp.prototype.maybe = function (pattern) {
        var p = pattern instanceof EngExp ? pattern.closeCaps() : pattern;
        this.pattern += "(?:" + EngExp.sanitize(p) + ")?";
        return this;
    };
    EngExp.prototype.anythingBut = function (characters) {
        this.pattern += "[^" + EngExp.sanitize(characters) + "]*";
        return this;
    };
    EngExp.prototype.digit = function () {
        this.pattern += "\\d";
        return this;
    };
    EngExp.prototype.repeated = function (from, to) {
        this.pattern = to ? "(?:" + this.pattern + "){" + from + "," + to + "}" : "(?:" + this.pattern + "){" + from + "}";
        return this;
    };
    EngExp.prototype.multiple = function (pattern, from, to) {
        var p = pattern instanceof EngExp ? pattern.closeCaps() : pattern;
        this.pattern += to ? "(?:" + EngExp.sanitize(p) + "){" + from + "," + to + "}" : "(?:" + EngExp.sanitize(p) + "){" + from + "}";
        return this;
    };
    EngExp.prototype.or = function (pattern) {
        // FILL IN HERE
        var p = pattern instanceof EngExp ? pattern.closeCaps() : pattern;
        this.pattern = "(?:" + this.pattern + "|(?:" + EngExp.sanitize(p) + "))";
        return this;
    };
    EngExp.prototype.beginCapture = function () {
        // FILL IN HERE
        this.pattern += "(";
        this.numberOfUnclosedCaps += 1;
        return this;
    };
    EngExp.prototype.endCapture = function () {
        // FILL IN HERE
        this.pattern += ')';
        return this;
    };
    EngExp.prototype.toString = function () {
        return this.asRegExp().source;
    };
    EngExp.prototype.valueOf = function () {
        return this.asRegExp().source;
    };
    EngExp.prototype.closeCaps = function () {
        while (this.numberOfUnclosedCaps > 0) {
            this.pattern += ')';
            this.numberOfUnclosedCaps -= 1;
        }
        return this;
    };
    return EngExp;
}());
exports.default = EngExp;
//# sourceMappingURL=engexp.js.map