- Your implementation of your error correction mechanism fixes capturing groups in the receivers but not at level 0 (that is, at the end of an EngExp without any nesting). Thus, the following produces a broken regex:  
    ``` javascript
    let e = new EngExp()
        .startOfLine()
        .beginCapture()
        .digit()
        .endOfLine()
        .asRegExp();
    ```
- Your error correction mechanism requires adding a check to every other operator of the language. This is a fairly involved interaction with other language features, thus brittle. A good alternative would be to check for broken capture groups only in `asRegExp()` and adjust the error correction rules appropriately.
- You forgot to decrement `numberOfUnclosedCaps` when a capture group is ended. Thus, the following perfectly balanced EngExp produces a broken regex:
    ``` javascript
    let e = new EngExp()
        .startOfLine()
        .beginCapture()
        .digit()
        .endCapture()
        .oneOrMore()
        .endOfLine()
        .asRegExp();
    ```
