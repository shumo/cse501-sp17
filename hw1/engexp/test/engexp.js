"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
var engexp_1 = require("../src/engexp");
describe("EngExp", function () {
    it("should parse a basic URL", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .anythingBut(" ")
            .endOfLine()
            .asRegExp();
        chai_1.expect(e.test("https://www.google.com/maps")).to.be.true;
    });
    it("should parse a disjunctive date pattern", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .digit().repeated(1, 2)
            .then("/")
            .then(new engexp_1.default().digit().repeated(1, 2))
            .then("/")
            .then(new engexp_1.default().digit().repeated(2, 4))
            .or(new engexp_1.default()
            .digit().repeated(1, 2)
            .then(" ")
            .then(new engexp_1.default().match("Jan").or("Feb").or("Mar").or("Apr").or("May").or("Jun")
            .or("Jul").or("Aug").or("Sep").or("Oct").or("Nov").or("Dec"))
            .then(" ")
            .then(new engexp_1.default().digit().repeated(2, 4)))
            .endOfLine()
            .asRegExp();
        chai_1.expect(e.test("12/25/2015")).to.be.true;
        chai_1.expect(e.test("25 Dec 2015")).to.be.true;
    });
    it("should capture nested groups", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .then("http")
            .maybe("s")
            .then("://")
            .maybe("www.")
            .beginCapture()
            .beginCapture()
            .anythingBut("/")
            .endCapture()
            .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        var result = e.exec("https://www.google.com/maps");
        chai_1.expect(result[1]).to.be.equal("google.com/maps");
        chai_1.expect(result[2]).to.be.equal("google.com");
    });
    it("should capture nested groups and work with disjunctions", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .then("http")
            .or("https")
            .then("://")
            .maybe("www.")
            .beginCapture()
            .beginCapture()
            .anythingBut("/")
            .endCapture()
            .anythingBut(" ")
            .endCapture()
            .endOfLine()
            .asRegExp();
        var result = e.exec("https://www.google.com/maps");
        chai_1.expect(result[1]).to.be.equal("google.com/maps");
    });
    it("should separate alternatives", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .digit().or("a")
            .then(" ")
            .then(new engexp_1.default().digit().or("b"))
            .or("z")
            .endOfLine()
            .asRegExp();
        chai_1.expect(e.test("b z"), "should not match 'b z'").to.be.false;
    });
    it("should support two kinds of nesting", function () {
        var e = new engexp_1.default()
            .startOfLine()
            .match("a")
            .or(new engexp_1.default().match("b").or("c").repeated(2, 3).then("d"))
            .asRegExp();
        chai_1.expect(e.test("a")).to.be.true;
    });
    it("should handle imbalance and oneOrMore", function () {
        var e = new engexp_1.default()
            .match("Hello")
            .beginCapture()
            .beginCapture()
            .match("World")
            .oneOrMore()
            .match("Fubar")
            .asRegExp();
        var result = e.exec("HelloWorldHelloWorldFubar");
        chai_1.expect(result[1]).to.be.equal("World");
    });
    it("should handle imbalance when nesting by passing argument", function () {
        var e = new engexp_1.default()
            .match("Hello")
            .or(new engexp_1.default()
            .beginCapture()
            .match("World"))
            .then("Fubar")
            .asRegExp();
        var result = e.exec("WorldFubar");
        chai_1.expect(result[1]).to.be.equal("World");
    });
    it("should reject missing beginCapture", function () {
        var e = new engexp_1.default()
            .match("Hello")
            .endCapture()
            .match("World")
            .oneOrMore()
            .match("Fubar");
        chai_1.expect(function () { return e
            .asRegExp(); })
            .to
            .throw("Invalid regular expression: /(?:(?:Hello))(?:World))+(?:Fubar)/: Unmatched ')'");
    });
});
//# sourceMappingURL=engexp.js.map